$(document).ready(function() {
$('.slider').owlCarousel({
        loop: true,
        center: false,
        margin: 0,
        stagePadding: 0,
        items: 1,
        pullDrag: false,
        autoplay: true,
        autoplayTimeout: 3000,
        navText: ['<i class="fa fa-arrow-left" aria-hidden="true"></i>', '<i class="fa fa-arrow-right" aria-hidden="true"></i>'],
        nav: false,
        lazyLoad: true,
        fluidSpeed: true,
        slideBy: 3,
        responsive: {
            0: {
                items: 1
            },
            600: {
                items: 3
            },
            1000: {
                items: 6
            }
        }
    });
        
}); // zakonzcenie document ready